/*
	Copyright (C) 2013 Lukasz Nidecki SQ5RWU

    This file is part of ArduinoQAPRS.

    ArduinoQAPRS is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    ArduinoQAPRS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ArduinoQAPRS; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    Ten plik jest częścią ArduinoQAPRS.

    ArduinoQAPRS jest wolnym oprogramowaniem; możesz go rozprowadzać dalej
    i/lub modyfikować na warunkach Powszechnej Licencji Publicznej GNU,
    wydanej przez Fundację Wolnego Oprogramowania - według wersji 2 tej
    Licencji lub (według twojego wyboru) którejś z późniejszych wersji.

    Niniejszy program rozpowszechniany jest z nadzieją, iż będzie on
    użyteczny - jednak BEZ JAKIEJKOLWIEK GWARANCJI, nawet domyślnej
    gwarancji PRZYDATNOŚCI HANDLOWEJ albo PRZYDATNOŚCI DO OKREŚLONYCH
    ZASTOSOWAŃ. W celu uzyskania bliższych informacji sięgnij do
    Powszechnej Licencji Publicznej GNU.

    Z pewnością wraz z niniejszym programem otrzymałeś też egzemplarz
    Powszechnej Licencji Publicznej GNU (GNU General Public License);
    jeśli nie - napisz do Free Software Foundation, Inc., 59 Temple
    Place, Fifth Floor, Boston, MA  02110-1301  USA

 */
/**
 * @file
 * @brief Konfiguracja bibliteki. @todo Zrobić to "The Arduino way"
 */
#ifndef QAPRSCONFIG_H_
#define QAPRSCONFIG_H_
#if defined(APRS_HW_TYPE_R2R)
	#include "QAPRSR2R.h"
	extern QAPRSR2R QAPRS;
#elif defined(APRS_HW_TYPE_Si4463)
	#include "QAPRSSi4463.h"
	extern QAPRSSi4463 QAPRS;
#elif defined(APRS_HW_TYPE_AD9850)
	#include "QAPRSAD9850.h"
	extern QAPRSAD9850 QAPRS;
#elif defined(APRS_HW_TYPE_TCM3105)
	#include "QAPRSTCM3105.h"
	extern QAPRSTCM3105 QAPRS;
#else
	#error Unsupported HW!
#endif


#endif /* QAPRSCONFIG_H_ */
